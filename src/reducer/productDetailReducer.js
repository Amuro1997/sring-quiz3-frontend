const initState = {
    productInfo:[]
}

const productDetailreducer = (previousState=initState, action)=>{
    console.log("in reducer");
    if(action.type === 'getProducts'){
        return ({
            ...previousState,
            productInfo: action.productInfo
        });
    }
    else
        return previousState;
}


export default productDetailreducer;