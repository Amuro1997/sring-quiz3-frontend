export const deleteOrderAction = (name) => {
    fetch(`http://localhost:8080/api/order/${name}`, {
        method: 'DELETE',
        body:{}
    }).then(response=>{
        console.log(response);
    }).catch((result)=>alert("cannot delete please retry"));
    return {
        type:''
    }
}
