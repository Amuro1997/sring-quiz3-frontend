
export const getProductsAction = () => (dispatch) => {
    console.log("In get Product");
    fetch("http://localhost:8080/api/product")
        .then(response=>response.json())
        .then(result=>{
            // console.log(result);
            dispatch({
            type:'getProducts',
            productInfo : result
        })})
}
