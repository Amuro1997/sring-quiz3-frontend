
export const createProductAction = (name, price, unit, url) => {
    // console.log("In create Product");
    fetch("http://localhost:8080/api/product",{
        headers: {
            'Content-Type': 'application/json'
        },
        method:'POST',
        body:JSON.stringify({name:name,price:price,unit:unit,url:url})
    }).then((response)=>console.log(response));
    return {
        type:''
    }
}
