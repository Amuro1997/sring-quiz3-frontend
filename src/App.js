import React, {Component} from 'react';
import './App.less';
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import Products from "./Components/Products";
import Orders from "./Components/Orders";
import AddProduct from "./Components/AddProduct";
class App extends Component{
  render() {
    return (
      <Router>
        <Route exact path={'/'} component={Products} />
        <Route exact path={'/orders'} component={Orders}/>
        <Route exact path={'/addProduct'} component={AddProduct}/>
      </Router>
    );
  }
}

export default App;