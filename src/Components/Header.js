import * as React from "react";
import {Link} from "react-router-dom";
import "./Header.less"
class Header extends React.Component{
    constructor(){
        super();
    }

    render(){
        return(
          <header>
              <ul id={"HeaderList"}>
                  <li><a><Link to="/">Store</Link></a></li>
                  <li><a><Link to="/orders">orders</Link></a></li>
                  <li><a><Link to="/addProduct">add</Link></a></li>
              </ul>
          </header>
        );
    }
}

export default Header;