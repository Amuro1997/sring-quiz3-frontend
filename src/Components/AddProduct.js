import {Component, default as React} from "react";
import Header from "./Header";
import {createProductAction} from "../actions/createProductAction";
import connect from "react-redux/es/connect/connect";
import "./AddProduct.less"

class AddProduct extends React.Component{

    constructor(){
        super();
        this.state = {
            name: null,
            price : null,
            unit: null,
            url:null,
            buttonStatus:false
        }

        this.performAddProductAction = this.performAddProductAction.bind(this);
        this.changeName = this.changeName.bind(this);
        this.changePrice = this.changePrice.bind(this);
        this.changeUnit = this.changeUnit.bind(this);
        this.changeUrl = this.changeUrl.bind(this);
    }

    performAddProductAction(){
        const{name, price, unit, url} = this.state;
        if(name == null || unit == null || url == null || isNaN(price)) {
            alert("Input not legal!");
            return;
        }
        if(this.props.productInfos.filter((product=>name===product.name)).length != 0)
            alert("Item already exist! Please enter new item name");
        else
            this.props.addProduction(name, price, unit, url);
    }

    changeName(event){
        console.log("changing name");
        this.setState({
            name:event.target.value
        })
    }

    changePrice(event){
        console.log("changing price");
        this.setState({
            price:event.target.value
        })
    }

    changeUnit(event) {
        console.log("changing unit");
        this.setState({
            unit: event.target.value
        })
    }

    changeUrl(event) {
        console.log("changing url");
        this.setState({
            url: event.target.value
        })
    }

    render(){
        return (
            <div id={"AddProduct"}>
                <header><Header/></header>
                <main id="AddProductMain">
                    <h2 id={"title"}>Add Product</h2>
                    <p>name</p><input value={this.state.name} onChange={this.changeName}/>
                    <p>price</p><input value={this.state.price} onChange={this.changePrice}/>
                    <p>unit</p><input  value={this.state.unit} onChange={this.changeUnit}/>
                    <p>url</p><input  value={this.state.url} onChange={this.changeUrl}/>
                    <button onClick={this.performAddProductAction}>submit</button>
                </main>
            </div>
        )
    }
}

const mapStateToProps = function(state){
    return {
        productInfos: state.products.productInfo
    }
}

const mapDispatchToProps = function(dispatch){
    return {
        addProduction: (name, price, unit, url)=>createProductAction(name, price, unit, url)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddProduct);