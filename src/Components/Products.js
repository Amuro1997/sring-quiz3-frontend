import React from "react";
import Header from "./Header";
import {getProductsAction} from "../actions/getProductsAction";
import connect from "react-redux/es/connect/connect";
import Product from "./Product";
import "./Product.less"
class Products extends React.Component{
    constructor(){
        super();
    }

    componentWillMount() {
        this.props.getAllProducts();
    }

    render(){
        if(this.props.productInfos.length == 0){
            console.log("Length is zeor");
            return <Header/>;
        }
        return (
            <div>
                <Header/>
                {this.props.productInfos.map((product,index)=>{
                    return <Product className="Product" index={index}/>
                })}
            </div>
        )
    }
}

const mapStateToProps = function(state){
    return {
        productInfos: state.products.productInfo
    }
}

const mapDispatchToProps = {
    getAllProducts : getProductsAction
}

export default connect(mapStateToProps, mapDispatchToProps)(Products);