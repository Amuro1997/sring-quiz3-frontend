import React, {Fragment} from "react";
import connect from "react-redux/es/connect/connect";
import "./Product.less"
import {createOrderAction} from "../actions/createOrderAction";
class Product extends React.Component{

    constructor(props) {
        super(props);
        this.performCreateorderAction = this.performCreateorderAction.bind(this);
    }

    performCreateorderAction(){
        const productInfo = this.props.productInfos[this.props.index];
        const{name} = productInfo;
        this.props.createOrder(name);
    }

    render(){
        if(this.props.productInfos.length === 0){
            return <Fragment></Fragment>;
        }

        const productInfo = this.props.productInfos[this.props.index];
        console.log(productInfo);
        return (
            <div id="productCard">
                <img src={productInfo.url}/>
                <h4>{productInfo.name}</h4>
                <h6>price:{productInfo.name}/{productInfo.unit}</h6>
                <button onClick={this.performCreateorderAction}>+</button>
            </div>
        )
    }

}

const mapStateToProps = function(state){
    return {
        productInfos: state.products.productInfo,
    }
}

const mapDispatchToProps = {
    createOrder:(name,price,unit,quantity)=>createOrderAction(name,price,unit,quantity)
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);