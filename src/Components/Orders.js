import React, {Component, Fragment} from "react";
import Header from "./Header";
import {getProductsAction} from "../actions/getProductsAction";
import connect from "react-redux/es/connect/connect";
import {getOrdersAction} from "../actions/getAllOrdersAction";
import {deleteOrderAction} from "../actions/deleteOrderAction";
import "./Order.less"
class Orders extends React.Component{
    constructor(props){
        super(props);
        this.generateOrderTable = this.generateOrderTable.bind(this);
        this.generateOrderInfo = this.generateOrderInfo.bind(this);
        this.performRemoveAction = this.performRemoveAction.bind(this);
    }

    componentWillMount() {
        this.props.getAllOrders();
    }

    performRemoveAction(name){
        this.props.deleteOrder(name);
    }

    generateOrderTable(){
        return(
            <table>
                {this.props.orderInfos.map(info=>this.generateOrderInfo(info))}
            </table>
        )
    }

    generateOrderInfo(info){
        const{name, price,quantity, unit} = info;
        const removeAction = ()=> this.performRemoveAction(name);
        return(
                <tr id = "OrderRoll">
                    <td>{name}</td>
                    <td>{price}</td>
                    <td>{quantity}</td>
                    <td>{unit}</td>
                    <td><button onClick={removeAction}>remove</button></td>
                </tr>
        )
    }

    render(){
        if(this.props.orderInfos.length === 0){
            return (
                <Fragment>
                    <header><Header/></header>
                    <h1 id="NoOrderMessage">No orders found please go back to store</h1>
                </Fragment>
            );
        }
        return (
            <div id={"Orders"}>
                <header><Header/></header>
                <main>
                    <section id="tableHeader">
                        <p>name</p>
                        <p>price</p>
                        <p>quantity</p>
                        <p>unit</p>
                        <p>operation</p>
                    </section>
                    {this.generateOrderTable()}
                </main>
            </div>
        )
    }
}

const mapStateToProps = function(state){
    return {
        orderInfos: state.orders.orderInfo
    }
}

const mapDispatchToProps = {
    getAllOrders: getOrdersAction,
    deleteOrder:(name)=>deleteOrderAction(name)
}

export default connect(mapStateToProps, mapDispatchToProps)(Orders);
