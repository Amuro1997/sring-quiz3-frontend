import {combineReducers} from "redux";
import productDetailreducer from "../reducer/productDetailReducer";
import OrderDetailreducer from "../reducer/OrderDetailReduer";


const reducers = combineReducers({
    products: productDetailreducer,
    orders: OrderDetailreducer,
});
export default reducers;